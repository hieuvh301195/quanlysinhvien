package com.example.model;

import java.io.Serializable;

public class Student implements Serializable {
    private String id;
    private String name;
    private boolean gender;
    private String avatar;

    public Student(){}

    public Student(String id, String name, boolean gender, String avatar) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMale() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
